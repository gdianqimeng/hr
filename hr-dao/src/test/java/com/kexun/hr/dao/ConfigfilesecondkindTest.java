package com.kexun.hr.dao;

import com.kexun.hr.model.Configfilesecondkind;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class ConfigfilesecondkindTest {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-mybatis.xml");
		ConfigfilesecondkindMapper configfilesecondkindMapper=(ConfigfilesecondkindMapper)context.getBean("configfilesecondkindMapper");
		List<Configfilesecondkind> findByFirstKindId = configfilesecondkindMapper.findByFirstKindId(2);
		for (Configfilesecondkind configfilesecondkind : findByFirstKindId) {
			System.out.println(configfilesecondkind);
		}
	}
}
